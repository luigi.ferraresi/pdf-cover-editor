import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { MergePdfComponent } from './components/merge-pdf/merge-pdf.component';
import { HttpClientModule } from '@angular/common/http';
import { PdfZipComponent } from './components/pdf-zip/pdf-zip.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MergePdfComponent,
    PdfZipComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
