import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PDFDocument, rgb, StandardFonts } from 'pdf-lib';
import { FormControl } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import * as JSZip from 'jszip';
import { HttpClient } from '@angular/common/http';

declare var require: any
const FileSaver=require('file-saver');

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  @ViewChild('coverUpload', { static: false }) myInputCover: ElementRef;
  @ViewChild('contentUpload', { static: false }) myInputContent: ElementRef;
  @ViewChild('retroUpload', { static: false }) myInputRetro: ElementRef;
  @ViewChild('logoUpload', { static: false }) myInputLogo: ElementRef;
  @ViewChild('stepper', { read: MatStepper }) stepper: MatStepper;

  name = new FormControl('');
  names: string[] = [];
  pdfCover: any;
  pdfCovers: any = [];
  coverName = '';
  pdfContent: any;
  contentName = '';
  pdfRetro: any;
  retroName = '';
  logoName = '';
  logo: any;
  allFilesUploaded = false;
  coverIsEdited = false;
  selectedOption = '';

  constructor(private snackBar: MatSnackBar,private httpClient: HttpClient) {}

  ngOnInit(): void {}

  onLogoSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      if (file.name.includes('.png') || file.name.includes('.jpg')) {
        const reader = new FileReader();
        reader.onload = (event: any) => {
          const logo = event.target.result;
          this.controlImageSize(logo, file);
        };
        reader.readAsDataURL(event.target.files[0]);
      } else {
        this.openSnackBar('Il tipo di file non è corretto', 'error');
      }
    }
  }

  async controlImageSize(logo: string, file: File) {
    const pdfDoc = await PDFDocument.load(this.pdfCover);
    const imageUrl = await fetch(logo);
    const imageBytes = await imageUrl.arrayBuffer();
    let image: any;

    if (file.type === 'image/png') {
      image = await pdfDoc.embedPng(imageBytes);
    } else if (file.type === 'image/jpeg') {
      image = await pdfDoc.embedJpg(imageBytes);
    }
    // if (image.width < 205 && image.width < 91) {
      this.logo = logo;
      this.logoName = file.name;
    // } else {
    //   this.resetLogo();
    //   this.openSnackBar('size della file non è corretto', 'error');
    // }
    // this.logoName = file.name;
  }

  onFileSelected(event: any, tipo: string) {
    const file: File = event.target.files[0];
    console.log(file.type)
    if (file && (file.type === 'application/pdf' || file.type === 'application/pkcs7-mime')) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        // this.url = event.target.result;
        switch (tipo) {
          case 'cover': {
            this.coverName = file.name;
            this.pdfCover = event.target.result;
            break;
          }
          case 'content': {
            this.pdfContent = event.target.result;
            this.contentName = file.name;
            break;
          }
          case 'retro': {
            this.pdfRetro = event.target.result;
            this.retroName = file.name;
            break;
          }
        }
      };
      reader.readAsDataURL(event.target.files[0]);
      // if (tipo === 'cover') {
      //   this.coverName = file.name;
      //   this.pdfCover = file;
      // } else if (tipo === 'content') {
      //   this.pdfContent = file;
      //   this.contentName = file.name;
      // } else if (tipo === 'retro') {
      //   this.pdfRetro = file;
      //   this.retroName = file.name;
      // }
      // if (this.pdfCover && this.pdfContent && this.pdfRetro) {
      //   this.allFilesUploaded = true;
      // }
    } else {
      this.openSnackBar('seleziona il tipo di file corretto', 'error');
    }
  }

  // modifies cover pdf with name or logo
  async modifyPdf() {


    const tag = {
      width: 155,
      height: 55
    };

    const tagPadding: number = 10;

    this.pdfCovers = [];
    if (this.logo) {
      //const existingPdfBytes = await this.pdfCover.arrayBuffer();
      const pdfDoc = await PDFDocument.load(this.pdfCover);
      const imageUrl = await fetch(this.logo);
      const imageBytes = await imageUrl.arrayBuffer();
      let image: any;

      if (this.logoName.includes('png')) {
        image = await pdfDoc.embedPng(imageBytes);
      } else if (this.logoName.includes('jpg')) {
        image = await pdfDoc.embedJpg(imageBytes);
      }

      const scaled = image.scaleToFit(tag.width, tag.height);

      console.log(scaled)
      
      const page = pdfDoc.getPage(0);
      page.drawImage(image, {
        x: page.getWidth() / 2 - scaled.width / 2,
        y: tagPadding,
        width: scaled.width,
        height: scaled.height
      });
      const pdfBytes = await pdfDoc.save();
      const file = new Blob([pdfBytes], { type: 'application/pdf' });
      this.pdfCovers.push(file);
    } else {
      this.names.forEach(async (name) => {


        const pdfDoc = await PDFDocument.load(this.pdfCover);
        const font = await pdfDoc.embedFont(StandardFonts.TimesRomanBold);
        const pages = pdfDoc.getPages();
        const firstPage = pages[0];
        const { width, height } = firstPage.getSize();
        
        const fontSize = 16;
        const textWidth = font.widthOfTextAtSize(name, fontSize);
        const textHeight = font.sizeAtHeight(fontSize);

        firstPage.drawText(name, {
          x: width / 2 - textWidth / 2,
          y: tag.height - textHeight,
          size: fontSize,
          font: font,
          color: rgb(0.204, 0.388, 0.62),
        });

        const pdfBytes = await pdfDoc.save();
        const file = new Blob([pdfBytes], { type: 'application/pdf' });
        this.pdfCovers.push(file);
      });
    }
    this.coverIsEdited = true;
  }

  showCover() {
    this.pdfCovers.forEach((elem: Blob | File) => {
      const fileUrl = URL.createObjectURL(elem);
      window.open(fileUrl);
    });
    // this.createZip(this.pdfCovers)
    // this.downloadZip()
  }

  downloadZip(){
    var zip = new JSZip();
    //zip.file("Hello.txt", "Hello World\n");

    var pdf = zip.folder("pdfs");


    this.pdfCovers.forEach((value: any,i: any) => {
      pdf?.file(i+'.pdf',value, { base64: true });
    });

    zip.generateAsync({ type: "blob" }).then(function (content) {
      FileSaver.saveAs(content, "example.zip");
    });
  }

  async createZip(files: any[]) { 

    const zip = new JSZip();  
    const name = 'zipName' + '.zip';  
    // tslint:disable-next-line:prefer-for-of  
    for (let counter = 0; counter < files.length; counter++) {  
      const element = files[counter];  
      const fileData: any = await this.getFile(element);  
      const b: any = new Blob([fileData], {
         type: '' + fileData.type + '' });    
     // zip.file(element.substring(element.lastIndexOf('/') + 1),b, {base64: true});  
    }  
    zip.generateAsync({ type: 'blob' }).then((content) => {  
      if (content) {  
        FileSaver.saveAs(content, name);  
      }  
    });  
  }  

  async getFile(url: string) {  
    const httpOptions = {  
      responseType: 'blob' as 'json'  
    };  
   return  this.httpClient.get(url, httpOptions).subscribe(res => 
      {
        return res})  
  } 

  resetCover() {
    this.myInputCover.nativeElement.value = '';
    this.pdfCover = null;
    this.coverName = '';
    this.pdfCovers = [];
  }

  resetContent() {
    this.myInputContent.nativeElement.value = '';
    this.pdfContent = null;
    this.contentName = '';
  }

  resetRetro() {
    this.myInputRetro.nativeElement.value = '';
    this.pdfRetro = null;
    this.retroName = '';
  }

  resetLogo() {
    if (this.logo) {
      this.myInputLogo.nativeElement.value = '';
      this.logo = null;
      this.logoName = '';
    }
  }

  addName() {
    const name = this.name.value;
    if (name && !this.names.includes(name)) {
      this.names.push(name);
      this.name.reset();
    }
  }

  removeName(name: string): void {
    const index = this.names.indexOf(name);
    if (index >= 0) {
      this.names.splice(index, 1);
    }
  }

  resetAll() {
    this.resetCover();
    this.resetContent();
    this.resetRetro();
    this.resetLogo();
    this.names = [];
    this.coverIsEdited = false;
    this.stepper.reset();
    this.selectedOption = '';
  }

  selectChanges() {
    this.selectedOption === 'Nome' ? this.resetLogo() : (this.names = []);
    // if(this.selectedOption === 'Nome'){
    //   this.logo?this.resetLogo():null;
    // }else {this.names = []}
  }

  openSnackBar(message: string, action?: string): void {
    this.snackBar.open(message, action, { duration: 3000 });
  }
}
