import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import * as JSZip from 'jszip';
import { PDFDocument } from 'pdf-lib';

declare var require: any
const FileSaver=require('file-saver');
@Component({
  selector: 'app-merge-pdf',
  templateUrl: './merge-pdf.component.html',
  styleUrls: ['./merge-pdf.component.scss'],
})
export class MergePdfComponent implements OnInit {
  @Input() coverPdfs: any[];
  @Input() contentPdf: any;
  @Input() retroPdf: any;
  @Input() coverEdited = false;

  pdfs:any[] = []

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'coverPdfs': {
            console.log('covers:',this.coverPdfs);
            break;
          }
          case 'contentPdf': {
            console.log('content:',this.contentPdf);
            break;
          }
          case 'retroPdf': {
            console.log('retro',this.retroPdf);
            break;
          }
          case 'coverEdited': {
            console.log('coveredited');
            break;
          }
        }
      }
    }
  }

  ngOnInit(): void {}

  async mergePdf() {
    // convert pdf files to array
    //const arrayB = await this.contentPdf.arrayBuffer();
    const secondDoc = await PDFDocument.load(this.contentPdf);
    //const arrayC = await this.retroPdf.arrayBuffer();
    const thirdDoc = await PDFDocument.load(this.retroPdf);
    
    this.coverPdfs.forEach(async (cover) => {
      // create pdfDoc
      const pdfDoc = await PDFDocument.create();

      // add pages of cover.pdf array to final pdf
      const arrayA = await cover.arrayBuffer();
      const firstDoc = await PDFDocument.load(arrayA);
      const firstPage = await pdfDoc.copyPages(
        firstDoc,
        firstDoc.getPageIndices()
      );
      firstPage.forEach((page) => pdfDoc.addPage(page));

      // add pages of content.pdf array to final pdf
      const secondPage = await pdfDoc.copyPages(
        secondDoc,
        secondDoc.getPageIndices()
      );
      secondPage.forEach((page) => pdfDoc.addPage(page));

      // add pages of retro.pdf array to final pdf
      const thirdPage = await pdfDoc.copyPages(
        thirdDoc,
        thirdDoc.getPageIndices()
      );
      thirdPage.forEach((page) => pdfDoc.addPage(page));

      // merge all pdfs into one
      const pdfBytes = await pdfDoc.save();
      let file = new Blob([pdfBytes], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      this.pdfs.push(file);
      window.open(fileURL);
    });
    console.log('pdfs: ',this.pdfs);
    //this.downloadZip();
  }

  downloadZip(){
    let zip = new JSZip();
    let pdf = zip.folder("pdfs");
    this.pdfs.forEach((value: any,i: any) => {
      pdf?.file(i+'.pdf',value, { base64: true });
    });

    zip.generateAsync({ type: "blob" }).then(function (content) {
      FileSaver.saveAs(content, "pdfs.zip");
    });
  }

  // async getFile(url: string) {  
  //   const httpOptions = {  
  //     responseType: 'blob' as 'json'  
  //   };  
  //   const res = await this.httpClient.get(url, httpOptions).toPromise().catch((err: HttpErrorResponse) => {  
  //     const error = err.error;  
  //     return error;  
  //   });  
  //   return res;  
  // }  

  async createZip(files: any[]) { 

    const zip = new JSZip();  
    const name = 'zipName' + '.zip';  
    // tslint:disable-next-line:prefer-for-of  
    for (let counter = 0; counter < files.length; counter++) {  
      const element = files[counter];  
      // const fileData: any = await this.getFile(element);  
      // const fileData: any = await this.pdfs;  
      // const b: any = new Blob([fileData], { type: '' + fileData.type + '' });  
      zip.file(element.substring(element.lastIndexOf('/') + 1));  
    }  
    zip.generateAsync({ type: 'blob' }).then((content) => {  
      if (content) {  
        FileSaver.saveAs(content, name);  
      }  
    });  
  }  

}
