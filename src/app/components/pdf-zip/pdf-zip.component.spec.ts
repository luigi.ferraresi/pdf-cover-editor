import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfZipComponent } from './pdf-zip.component';

describe('PdfZipComponent', () => {
  let component: PdfZipComponent;
  let fixture: ComponentFixture<PdfZipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PdfZipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfZipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
